bert->Examples->Inversion->3dTopo->burialMound
###############################################

This example is the only synthetic data set in the tutorial.
It is the hypothetic burial mound presented by G�nther et al. (2006), the original BERT paper.
It comprises 9 profiles in x direction and 13 profiles in y direction with a modifies dipole-dipole array.
The synthetic data are contaminated with noise, i.e. they should be fitted with chi^2=1.

G�nther, T., R�cker, C., and Spitzer, K. (2006). 3-d modeling and inversion of DC resistivity
data incorporating topography - Part II: Inversion. Geophys. J. Int., 166(2):506-517.