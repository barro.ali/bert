#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import os
import numpy as np
import pygimli as pg
from pygimli.physics import ert


def createMixMesh(verbose=True):
    """Create a triangle prism mesh."""
    xdim = [0, 20, 2.5]
    ydim = [0, 32.5, 2.5]
    # dxy = [2.5, 2.5]
    paraBoundary = 2.5
    boundary = 50
    depth = 10
    x = x=np.arange(xdim[0]-paraBoundary, xdim[1]+paraBoundary+.1, xdim[2]*1.0)
    y = y=np.arange(ydim[0]-paraBoundary, ydim[1]+paraBoundary+.1, ydim[2]*1.0)
    grid = pg.createGrid(x, y)

    # for b in grid.boundaries():
    #     b.setMarker(1)

    for c in grid.cells():
        c.setMarker(2)

    # grid.translate((0.0, -grid.ymax() - boundary))
    pg.show(grid, markers=True)

    mesh2 = pg.meshtools.appendTriangleBoundary(grid,
                                                xbound=boundary,
                                                ybound= boundary,
                                                quality=30,
                                                marker=1, isSubSurface=False)

    mesh2.smooth(True, False, 1, 2)
    # mesh2.translate((0.0, -grid.ymax() + ydim[1]))

    for b in mesh2.boundaries():
        if b.marker() < 0:
            b.setMarker(pg.core.MARKER_BOUND_MIXED)

    pg.show(mesh2, markers=True)

    mesh3 = pg.meshtools.createMesh3D(
        mesh2,
        -pg.cat(np.arange(0, 12.6, 2.5/2),
                pg.utils.grange(2.5, boundary, n=10) + 12.5),
        pg.core.MARKER_BOUND_HOMOGEN_NEUMANN,
        pg.core.MARKER_BOUND_MIXED)

    for c in mesh3.cells():
        if c.marker() == 2:
            if c.center()[2] < -depth: c.setMarker(1)

    return mesh3

mesh = createMixMesh()
mesh.exportVTK("mesh.vtk")
data = pg.load("gallery.dat")
data["err"] = ert.estimateError(data)
mgr = ert.ERTManager(data)
mgr.invert(mesh=mesh, verbose=True)
mgr.saveResult()
# os.mkdir()
# mesh.save("mesh/mesh")
