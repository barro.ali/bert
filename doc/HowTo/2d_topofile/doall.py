#!/bin/env python

import numpy as np
import pygimli as pg
import pybert as pb

infile='profile-topo.dat'
outfile='profile.dat'

data = pb.DataContainerERT(infile)
print(data)
topo  = data.additionalPoints()

A = np.array([pg.x(topo), pg.z(topo)])

tt = np.hstack((0., np.cumsum(np.sqrt(np.diff(A[0])**2 + np.diff(A[1])**2))))
xe = [pos[0] for pos in data.sensorPositions()]
z  = np.interp(xe, A[0], A[1])
x  = xe[0] + np.hstack((0., np.cumsum(np.sqrt(np.diff(xe)**2 - np.diff(z)**2))))
de = np.sqrt(np.diff(x)**2 + np.diff(z)**2)

for i in range(data.sensorCount()):
    data.setSensorPosition(i, [x[i], 0.0, z[i]])

data.save(outfile,'a b m n u i r','x z')

