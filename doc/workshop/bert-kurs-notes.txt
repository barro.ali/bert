Unterschied zu alten Workshops

Noch st�rkerer Fokus auf pyGIMLi
    (als Tool zum BERT Pr�/Post-Prozessing)
    Einf�hrung in Grundkonzepte
mehr Zeit zum "�ben"
konkrete Fragestunde f�r spezifische Probleme
St�rkerer Fokus auf Timelapse
Mehr �ber IP (Varianten, )
Resistivity Manager (BERT 2-->3)
SIP Manager (s. a. Poster)
Fehlermodell und Fehlersch�tzung
Vorw�rtsmodellierung
Different levels (equation, physics, manager, app)


Noch etwas unklar

�bersicht (Bilder aus Papers) noch sinnvoll?
Entwicklung von BERT (Richtung 3)
    ohne Python noch sinnvoll (nur Grundfunktionen)
    ausschlie�lich �ber Manager (ERT.loadCFG)?
    oder mehr Python-Apps (TL proz & inv)
    TL (Framework) integriert oder eig. Manager?
Auf Poly2D Klasse f�r synth. Mod. eingehen?


Todo-Liste

Ank�ndigungsmail mit Frage nach Themenschwerpunkt
kurz vorher Email:
    Link auf Windows-Installer mit zugeh. WinPython
    (Version 2.1)
    Link auf gitlab Repo (account n�tig)
    
Tutorial auf Stand 2.1 bringen
    Integration migration guide
    flexiblerer Umgang
    
Referenzen updaten
�bersichtsbild Mesh-Generierung
Daten und Modelle aus Papers in Repo


IP-Inversion

BERT1-linearisierte (log rhoa - phi) Inversion
BERT2 imag. Res. Inversion (mit Approx)
Chargeability-Inversion
spektrale Inversion 