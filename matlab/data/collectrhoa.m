function [R,Rrez]=collectrhoa(Data,MEA,rhoBg)

% FDFWD3D - 3D DC Forward Calculation with finite differences
% Rhoa = collectrhoa(Data,MEA,rhoBg)
% [Rhoa_abmn,Rhoa_mnab] = ...
%  Data    - Structure of electrode numbers(a,b,m,n), 
%            k-factors(k) and measurements(r)
%            elec- Electrode Positions
%  MEA - multielectrode potential matrix
%        matrix of (size(Data.elec,1))^2

if nargin<2, error('Two input arguments required!'); end
if nargin<3, rhoBg=0; end
% hacked!!!!!!!!!
if length(rhoBg)>1,
    for e=1:size(MEA,1),
        dx = Data.elec(:,1) - Data.elec(e,1);
        dy = Data.elec(:,2) - Data.elec(e,2);
        dxdy2 = dx.^2 + dy.^2;
        dz = Data.elec(:,3) - Data.elec(e,3);
        dz1 = Data.elec(:,3) + Data.elec(e,3);
        MEA(:,e) = MEA(:,e) + ( 1./sqrt(dxdy2+dz.^2) + 1./sqrt(dxdy2+dz1.^2) ) / 4 / pi / rhoBg(e);
%         ele(e,1)=1;%???
%         MEA(:,e)=MEA(:,e)+1./sqrt(sum(ele.^2,2))/2/pi*rhoBg(e);
    end
end

data = length( Data.a );
R = zeros( size( Data.a ) );
Rrez = R;
for l = 1:data,
    R(l) = MEA( Data.a(l), Data.m(l) );
    Rrez(l) = MEA( Data.m(l), Data.a(l) );
    if Data.n(l)>0, 
        R(l) = R(l) - MEA( Data.a(l), Data.n(l) ); 
        Rrez(l) = Rrez(l) - MEA( Data.n(l),Data.a(l) ); 
    end
    if Data.b(l)>0,
        R(l) = R(l) - MEA( Data.b(l), Data.m(l) );
        Rrez(l) = Rrez(l) - MEA(Data.m(l), Data.b(l) );
        if Data.n(l)>0, 
            R(l) = R(l) + MEA( Data.b(l), Data.n(l) ); 
            Rrez(l) = Rrez(l) + MEA( Data.n(l), Data.b(l) ); 
        end
    end
end
if isfield( Data, 'k' ),
    R(:) = R(:) .* Data.k(:);
    Rrez(:) = Rrez(:) .* Data.k(:);
else
    display('warning! no geometric factors present in data file!');
end
if length(rhoBg)==1,
    R=R+rhoBg;
    Rrez=Rrez+rhoBg;
end
if nargout<2,
    rez=(R-Rrez)*2./(R+Rrez);
    messg(sprintf('Standard deviation of reciprocity %.2f%%',std(rez)*100));
    R=sqrt(abs(R.*Rrez));
end
