// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#include "plane.h"
#include "stlmatrix.h"
#include "solver.h"

using namespace MyVec;

namespace MyMesh{
ostream & operator << ( ostream & str, const Plane & p ){
  if ( p.valid() ){
    str << "Plane: norm = " << p.norm() << " p = " << p.d() << " ";
  } else {
    str << "Plane: invalid ";
  }
  return str;
}

Plane::Plane( ){
  valid_ = false;
}

Plane::Plane( const RealPos & norm, double d )
: norm_( norm ), d_( d ){
  valid_ = false;
  checkValidity();
}

Plane::Plane( const RealPos & norm, const RealPos & p0 ){
  valid_ = false;
  norm_ = norm;
  d_ = p0.abs();
  checkValidity();
}

Plane::Plane( const RealPos & p0, const RealPos & p1, const RealPos & p2 ){
  valid_ = false;

  norm_ = p0.norm( p1, p2 ) ;

  STLVector one( 3 ); one[ 0 ] = 1.0; one[ 1 ] = 1.0; one[ 2 ] = 1.0; 
  STLMatrix A( 3, 3);
  A[ 0 ]= p0.vec();  A[ 1 ] = p1.vec();  A[ 2 ] = p2.vec();

  STLMatrix A0( A ); A0.setColumn( 0, one );
  STLMatrix A1( A ); A1.setColumn( 1, one );  
  STLMatrix A2( A ); A2.setColumn( 2, one );

  double detA = det( A ); 
  if ( fabs( det( A ) ) < TOLERANCE ){
    d_ = 0.0;
  } else {
    d_ = detA / ( sqrt( pow( det( A0 ), 2.0 ) + pow( det( A1 ), 2.0 ) + pow( det( A2 ), 2.0 ) ) );
  }

  //  d_ = rint( d_ / ( TOLERANCE * 1e-2 ) ) * TOLERANCE * 1e-2;
  //  norm_.round();
  checkValidity();
}

Plane::Plane( double a, double b, double c, double d ){
  valid_ = false;
  double abstmp = sqrt( a*a + b*b + c*c );
  norm_ = RealPos( a / abstmp, b / abstmp, c / abstmp );
  d_ = d / abstmp;
  checkValidity();
}

Plane::Plane( const Plane & plane )
: norm_( plane.norm() ), d_( plane.d() ), valid_( plane.valid() ) {
}

Plane::~Plane(){ }

Plane & Plane::operator = ( const Plane & plane ){ 
  if ( this != & plane ){
    norm_ = plane.norm();
    d_ = plane.d();
    valid_ = plane.valid();
  } return *this;
}      

bool Plane::operator == ( const Plane & plane ){
  return compare( plane );
}

bool Plane::operator != ( const Plane & plane ){
  return !compare( plane );
}

bool Plane::checkValidity(){
  if ( fabs( norm_.abs() - 1.0 ) < TOLERANCE ){
    valid_ = true; 
  } else { 
    cerr << WHERE_AM_I << " WARNING! Plane NOT valid " << fabs( norm_.abs() - 1.0 ) << " / " 
	 << TOLERANCE << endl;  
    valid_ = false; 
  }
  return false;
}

int Plane::touch( const RealPos & pos, double tol ){ 
  //  cout << WHERE_AM_I << fabs( this->distance( pos ) ) << endl;
  if ( valid_ ){
    return ( fabs( this->distance( pos ) ) < tol ); 
  } else return false;
}

RealPos Plane::intersect( const Line & line, double tol ){
  //** http://astronomy.swin.edu.au/~pbourke/geometry/planeline/;
  if ( !this->valid() || !line.valid() ) return RealPos();
  //** the Plane and the Line are coplanar;
  RealPos p0( line.p0().vec() );
  RealPos p1( line.p1().vec() );

  if ( this->touch( p0, tol ) && this->touch( p1, tol ) ) return RealPos();

  RealPos norm( this->norm() );
  RealPos x0( this->x0() );
  
  double t = norm.scalar( x0 - p0 ) / norm.scalar( p1 - p0 );
  //  double t = norm.scalar( p3 - p1 ) / norm.scalar( p2 - p1 );

  if ( isnan( t ) || isinf( t ) ) {
    //    cerr << WHERE_AM_I << " t = " << t << endl;
    return RealPos();
  }

  return RealPos( p0 + ( p1 - p0 ) * t );
}

Line Plane::intersect( const Plane & plane, double tol ){
  //** both planes are identical
  if ( (*this) == plane ) return Line();
  
  STLVector n0 = norm().vec();
  STLVector n1 = plane.norm().vec();
  STLVector a = RealPos( n0 ).cross( RealPos( n1 ) ).vec(); 

  //** both planes are parallel;
  if ( isZero( a ) ) return Line();

  //p = c1 N1 + c2 N2 + u N1 * N2 // line definition;
  double n0n0 = scalar( n0, n0 );
  double n0n1 = scalar( n0, n1 );
  double n1n1 = scalar( n1, n1 );
  double det = n0n0 * n1n1 - n0n1 * n0n1;
  double d0 = this->d();
  double d1 = plane.d();

  double c0 =  ( d0 * n1n1 - d1 * n0n1 ) / det;
  double c1 =  ( d1 * n0n0 - d0 * n0n1 ) / det;
  
  RealPos lineP0( n0 * c0 + n1 * c1 );
  RealPos lineP1( a + lineP0.vec() );
  
  return Line( lineP0, lineP1 );
}

bool Plane::compare( const Plane & plane, double tol ){
  tol = 1e-4;
  if ( ( norm_-plane.norm() ).abs() < tol && fabs( d_ - plane.d() ) < tol ) return true; else return false;
}

} //namespace MyMesh

/*
$Log: plane.cpp,v $
Revision 1.3  2008/02/28 11:52:51  carsten
*** empty log message ***

Revision 1.2  2005/08/09 18:13:44  carsten
*** empty log message ***

Revision 1.1  2005/01/12 20:32:40  carsten
*** empty log message ***

*/
