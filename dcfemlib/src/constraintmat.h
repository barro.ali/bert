// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#ifndef CONSTRAINTMAT__H
#define CONSTRAINTMAT__H CONSTRAINTMAT__H

#include "dcfemlib.h"
using namespace DCFEMLib;

#include "myvec/vector.h"
#include "myvec/matrix.h"
using namespace MyVec;

#include "basemesh.h"

DLLEXPORT vector < BaseElement * > findParaBoundarys( const BaseMesh & mesh, int marker );
DLLEXPORT int create1stOrderConstraintsNew( SMatrix & C, const vector < BaseElement * > & bounds);
DLLEXPORT int createBoundaryWeightVector( const vector < BaseElement * > & bounds, RVector & boundWeight, double powz );

DLLEXPORT int create0thOrderConstraints( RDirectMatrix & C );
DLLEXPORT int create0thOrderConstraints( RDirectMatrix & C, const RVector & vec );
DLLEXPORT int create1stOrderConstraints( RDirectMatrix & C, const BaseMesh & meshPara);
DLLEXPORT int create1stOrderConstraintsRobust( RDirectMatrix & C, const BaseMesh & meshPara, const RVector & model);
DLLEXPORT int create1stOrderConstraintsArea( RDirectMatrix & C, const BaseMesh & meshPara );
DLLEXPORT int create2ndOrderDirichletConstraints( RDirectMatrix & C, const BaseMesh & meshPara );
DLLEXPORT int create2ndOrderNeumannConstraints( RDirectMatrix & C, const BaseMesh & meshPara );
DLLEXPORT int create2ndOrderMixedConstraints( RDirectMatrix & C, const BaseMesh & meshPara );

#endif // CONSTRAINTMAT__H

/*
$Log: constraintmat.h,v $
Revision 1.8  2006/10/18 17:53:27  thomas
no message

Revision 1.7  2006/09/25 11:31:49  carsten
*** empty log message ***

Revision 1.6  2006/09/22 10:42:18  thomas
DLLEXPORT added

Revision 1.5  2006/02/07 15:17:22  thomas
new constraint formulation - first steps

Revision 1.4  2006/02/06 17:53:54  carsten
*** empty log message ***

Revision 1.3  2006/02/06 17:30:42  carsten
*** empty log message ***

Revision 1.2  2005/04/12 15:39:05  carsten
*** empty log message ***

Revision 1.1  2005/04/12 11:41:09  carsten
*** empty log message ***
*/
