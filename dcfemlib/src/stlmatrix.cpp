// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//  
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//  
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//  
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//  

#include "stlmatrix.h"
#include "myvec/vector.h"

// #include <iostream>
// #include <fstream>

using namespace std;

namespace MyVec{

int LinSparseVector::save( const string & fname ){
  FILE *file;
  file = fopen( fname.c_str(), "w+b" );
  if ( !file ) return 0 ;

  long row = this->rows();
  long col = this->cols();
  long nVals = this->size();
  
  //  cout << " save " << row << " " << col << endl;

  fwrite( (char*)&row, sizeof( long ), 1, file );
  fwrite( (char*)&col, sizeof( long ), 1, file );
  fwrite( (char*)&nVals, sizeof( long ), 1, file );

  for ( int i = 0; i < nVals; i++ ) {
    if ( isinf( (*this)[i].val) ){
      cerr << "Spsarse.save " << (*this)[i].row << " " << (*this)[i].col << " " << (*this)[i].val << endl;
    }
    fwrite( (char*)&(*this)[ i ], sizeof( linSparseMatElement ), 1, file );
  }

  fclose( file ); 
  return 1;
}
  
int LinSparseVector::save( const string & fname, IOFormat format ){
  fstream file; openOutFile( fname, &file );
  
  for ( uint i = 0; i < this->size(); i ++ ){
    file << (*this)[i].row << " " << (*this)[i].col << " " << (*this)[i].val << endl;
  }
  
  file.close();
  return 1;
}

int LinSparseVector::load( const string & fname ){
  this->clear();
  FILE *file;
  file = fopen( fname.c_str(), "r+b" );
  if ( !file ) return 0 ;

  long nVals= 0, row = 0, col = 0;

  fread( &row, sizeof( long ), 1, file );
  fread( &col, sizeof( long ), 1, file );
  fread( &nVals, sizeof( long ), 1, file );

  this->setRows( row );
  this->setCols( col );
  this->resize( nVals );

  //  cout << "load " << row << " " << col << endl;
  for ( int i = 0; i < nVals; i++ ) {
    fread( &(*this)[ i ], sizeof( linSparseMatElement ), 1, file );
    if ( isinf( (*this)[ i ].val) ){
      cerr << "Spsarse.load " << (*this)[i].row << " " << (*this)[i].col << " " << (*this)[i].val << endl;
    }
  }
  
  fclose( file ); 
  return 1;

}

STLMatrix::STLMatrix( size_t d1, size_t d2 ) : dim1_( d1 ), dim2_( d2 ) {
  mat_ = new valarray < valarray< double > >( valarray< double >( dim2_ ), dim1_ );
  clean(); 
}
  /*!Constructs a STLMatrix that is a copy of STLMatrix a.*/
STLMatrix::STLMatrix( const STLMatrix & a ) : dim1_( a.dim1() ), dim2_( a.dim2() ) {
  mat_ = new valarray < valarray< double > >( a.matrix() );
}

void STLMatrix::resize( int dim1, int dim2 ) {
  dim1_ = dim1; dim2_ = dim2;
  delete mat_;
  mat_ = new valarray < valarray< double > >( valarray< double >( dim2_ ), dim1_ );

//   for ( int i = 0; i < dim1_; i ++ ){
//     (*mat_)[ i ].resize( dim2_ );
//   }
//   mat_->resize( dim1_ );
}

void STLMatrix::clean(){
  for ( size_t i = 0; i < dim1_; i ++ ){
    for ( size_t j = 0; j < dim2_; j ++ ){
    (*mat_)[ i ][ j ] = 0.0;
    }
  }
}

/*!Assigns a shallow copy of STLMatrix a and returns a reference to this STLMatrix.*/
STLMatrix & STLMatrix::operator = ( const STLMatrix & A ){
  if ( this != &A ){
    dim1_ = A.dim1();
    dim2_ = A.dim2();
    delete mat_;
    mat_ = new valarray < valarray< double > >( A.matrix() );
  } return *this;
}
  
STLMatrix & STLMatrix::operator += ( const STLMatrix & A ){
  for ( size_t i = 0; i < dim1_; i ++ ){
    for ( size_t j = 0; j < dim2_; j ++ ){
      (*mat_)[ i ][ j ] += A[ i ][ j ];
    }
  }
  return *this;
}

STLVector STLMatrix::column( size_t col ) const {
  STLVector tmp( dim1_ );
  for ( size_t i = 0; i < dim1_; i ++ ){
    tmp[ i ] = (*mat_)[ i ][ col ];
  }
  return tmp;
}

void STLMatrix::setColumn( size_t col, const valarray< double > & vec ){
  if ( vec.size() < dim1_ + 1 ){ 
    for ( size_t i = 0; i < dim1_; i ++ ){
      (*mat_)[ i ][ col ] = vec[ i ];
    }
  }
}

void STLMatrix::setRow( size_t row, const valarray< double > & vec ){
  if ( vec.size() < dim1_ + 1 ){ 
    for ( size_t i = 0; i < dim1_; i ++ ){
      (*mat_)[ row ][ i ] = vec[ i ];
    }
  }
}

// STLMatrix & STLMatrix::fill( double a[][] ){
//   cout << a << endl;
//   for ( size_t i = 0; i < dim1_; i ++ ){
//     for ( size_t j = 0; j < dim2_; j ++ ){
//       cout << i << j << a[ i ][ j ] <<endl;
//       (*mat_)[ i ][ j ] = a[ i ][ j ];
//     }
//   }
//   return *this;
// }

STLMatrix operator + ( const STLMatrix & A, const STLMatrix & B ){
  STLMatrix tmp( A );
  return tmp += B;
}

STLMatrix operator * ( const STLMatrix & A, const STLMatrix & B ){
  STLMatrix tmp( A.dim1(), A.dim2() );

  for( int i = 0, imax = tmp.dim1(); i < imax; i++ ){
    for( int j = 0, jmax = tmp.dim2(); j < jmax; j++ ){
      tmp[ i ][ j ] = scalar( A.row( i ), B.column( j ));
    }
  }
  return tmp;
}


STLMatrix operator * ( const STLMatrix & M, double val ){
  STLMatrix tmp( M );
  return tmp *= val;
}
STLMatrix operator * ( double val, const STLMatrix & M ){
  STLMatrix tmp( M );
  return tmp *= val;
}

STLMatrix & STLMatrix::operator *= ( double val ){
  for ( size_t i = 0; i < dim1_; i ++ ){
    (*mat_)[ i ] *= val;
  }
  return *this;
}

STLMatrix & STLMatrix::operator /= ( double val ){
  for ( size_t i = 0; i < dim1_; i ++ ){
    (*mat_)[ i ] /= val;
  }
  return *this;
}

ostream & operator << ( ostream & str, const STLVector & a ){
  str << "STLVector: (" << a.size() <<  "): " << endl << "\t";
  for ( int i = 0, imax = a.size(); i < imax; i ++ ){
    str << a[ i ] << "\t";
  }
  str << endl;
  return str;
}

ostream & operator << ( ostream & str, const STLMatrix & a ){
  str << "STLMatrix: (" << a.dim1() << "," <<  a.dim2() <<  "): " << endl;
  for ( size_t i = 0, imax = a.dim1(); i < imax; i ++ ){
    str << "\t";
    for ( size_t j = 0, imax = a.dim2(); j < imax; j ++ ){
      str << a[ i ][ j ] << "\t";
    }
     str << endl;
  }
  return str;
}

// double mul( const valarray< double > & v1, const valarray< double > & v2 ){
//   double result = 0.0;
//   for ( size_t i = 0, imax = v2.size(); i < imax; i ++ ){
//     result += v1[ i ] * v2[ i ];
//   }
//   return result;
// }

valarray< double > operator * ( const STLMatrix & A, const valarray< double > & b ){
  valarray < double > result( A.dim1() );
  for ( size_t i = 0, imax = A.dim1(); i < imax; i ++ ){
    result[ i ] = scalar( A.row( i ), b );
  }
  return result;
}

// STLMatrix operator*( const STLMatrix & A, double val ){
//   STLMatrix tmp( A );
//   return tmp *= val;
// }

}

/*
$Log: stlmatrix.cpp,v $
Revision 1.8  2006/09/21 17:08:40  carsten
*** empty log message ***

Revision 1.7  2006/07/27 14:52:08  carsten
*** empty log message ***

Revision 1.6  2006/07/19 19:20:18  carsten
*** empty log message ***

Revision 1.5  2006/02/14 11:09:18  carsten
*** empty log message ***

Revision 1.4  2005/10/17 11:56:58  carsten
*** empty log message ***

Revision 1.3  2005/10/07 17:19:49  carsten
*** empty log message ***

Revision 1.2  2005/10/06 17:38:36  carsten
*** empty log message ***

Revision 1.1  2005/01/04 19:22:30  carsten
add in cvs

*/
