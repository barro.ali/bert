// Copyright (C) 2005 Carsten R�cker <cruecker@uni-leipzig.de>
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include <dcfemlib.h>
#include <longoptions.h>
#include <iostream>

#include <domain3d.h>
#include <domain2d.h>

#include <spline.h>

using namespace std;
using namespace DCFEMLib;

int main( int argc, char * argv[] ){

  LongOptionsList lOpt;
  lOpt.setLastArg("[-COd:m:s:x:y:z:a:t:] polygon-filename");
  lOpt.setDescription( (string) "3D -- Create a simple hexahedron (dims: x,y,z) world with default boundary conditions: " +
		      "1 at surface (homogeneous Neumann = electric insulation) and 2 in subsurface (Mixed BC)."
		       " 2D -- Creates a simple Rectangle (dims: x,z) world with default boundary conditions. Size ( 0--xdim, [ 0--ydim ], -zdim--0 ). Optional you can add a list of points for topography." );
  lOpt.insert( "help", no_argument, 'h', "This help" );
  lOpt.insert( "verbose", no_argument, 'v', "Verbose mode" );
  lOpt.insert( "circle", no_argument, 'C', "Only with topofile, close topopolygon" );
  lOpt.insert( "objectOnly", no_argument, 'O', "bool: Object without world boundary markers. Used for merging with a real world" );
  lOpt.insert( "marker", required_argument, 'm', "int: Region marker for this world [1]" );
  lOpt.insert( "spline", required_argument, 's', "int: create cubic spline with n segments instead of one edges [ 1 ], (2D circle only)" );
  lOpt.insert( "xdim", required_argument, 'x', "double: x-size [1]" );
  lOpt.insert( "ydim", required_argument, 'y', "double: y-size [1]" );
  lOpt.insert( "zdim", required_argument, 'z', "double: z-size [1]" );
  lOpt.insert( "area", required_argument, 'a', "double: maximum size of tetrahedra [off == 0.0]" );
  lOpt.insert( "topofile", required_argument, 't', "string: file with list of topopoints 2D(x y); 3D(x y z)" );
  lOpt.insert( "dimension", required_argument, 'd', "int: Dimension [3]" );

  bool help = false, verbose = false, circle = false, objectOnly = false;
  int marker = 0, dimension = 3, splineSegments = 1;
  string topoFileName( NOT_DEFINED );
  string domainName( NOT_DEFINED );

  double xdim = 1.0, ydim = 1.0, zdim = 1.0, area = 0.0;
  int option_char = 0, option_index = 0, tracedepth = 0;
  while ( ( option_char = getopt_long( argc, argv, "?hvm:"
				       "O"
				       "C"
				       "s:" // spline segements
				       "x:"
				       "y:"
				       "z:"
				       "d:" // dimension
				       "t:" // topofileList
				       "a:", lOpt.long_options(), & option_index ) ) != EOF){
    switch ( option_char ) {
    case '?': lOpt.printHelp( argv[0] ); return 1; break;
    case 'h': lOpt.printHelp( argv[0] ); return 1; break;
    case 'C': circle = true; break;
    case 'O': objectOnly = true; break;
    case 'm': marker = atoi( optarg ); break;
    case 's': splineSegments = atoi( optarg ); break;
    case 'x': xdim = atof( optarg ); break;
    case 'y': ydim = atof( optarg ); break;
    case 'z': zdim = atof( optarg ); break;
    case 'a': area = atof( optarg ); break;
    case 'd': dimension = atoi( optarg ); break;
    case 't': topoFileName = optarg; break;
    case 'v': verbose = true; break;
    default : cerr << "default value not defined" << (char)option_char << endl;
    }
  }
  if ( argc < 2 ) { lOpt.printHelp( argv[0] ); return 1; }

  domainName = argv[argc-1];
  string domainFileName( domainName.substr( 0, domainName.rfind(".poly") ) + ".poly" );

    if ( dimension == 2 ){

    if ( topoFileName != NOT_DEFINED ){
      Domain2D world;

      //** read topography data;
      fstream file; if ( !openInFile( topoFileName, &file, true ) ){ return 1; }
      vector < string > row;
      vector < RealPos > topo;
      vector < int > electrodes;
      int markerTmp = 0;
      while ( !file.eof() ){
	row = getNonEmptyRow( file );
	switch( row.size() ){
	case 2:
	  topo.push_back( RealPos( toDouble( row[ 0 ] ), toDouble( row[ 1 ] ) ) );
	  markerTmp = 0;
	  break;
	case 3:
	  topo.push_back( RealPos( toDouble( row[ 0 ] ), toDouble( row[ 1 ] ) ) );
	  markerTmp = toInt( row[ 2 ] );
	  break;
	}
	electrodes.push_back( markerTmp );
      }
      file.close();

      //** start building poly from topography data;
      if ( ! circle ){
	world.createVIP( RealPos( topo.front() - RealPos( xdim, 0.0 ) ) );
      }
      world.createPolygon( createSpline( topo, splineSegments, circle ), HOMOGEN_NEUMANN_BC, circle );
        if ( ! circle ){ // 2d - close earth surface
          
	   world.createVIP( RealPos( topo.back() + RealPos( xdim, 0.0 ) ) );
	world.createVIP( RealPos( topo.back() + RealPos( xdim, -zdim ) ) );
	world.createVIP( RealPos( topo.front() + RealPos( - xdim, -zdim ) ) );
	double	zMin = min( world.node( world.nodeCount() -2 ).y(),
			    world.node( world.nodeCount() -1 ).y() );
	world.node( world.nodeCount() -2 ).setY( zMin );
	world.node( world.nodeCount() -1 ).setY( zMin );

	world.createEdge( world.node( 0 ), world.node( 1 ), HOMOGEN_NEUMANN_BC );
	world.createEdge( world.node( world.nodeCount() -4 ), world.node( world.nodeCount() -3 ), HOMOGEN_NEUMANN_BC );
	world.createEdge( world.node( world.nodeCount() -3 ), world.node( world.nodeCount() -2 ), MIXED_BC );
	world.createEdge( world.node( world.nodeCount() -2 ), world.node( world.nodeCount() -1 ), MIXED_BC );
	world.createEdge( world.node( world.nodeCount() -1 ), world.node( 0 ), MIXED_BC );
      }
      if ( marker > 0 ) {
	if ( circle ){
	  if ( marker > 0 ) world.createRegionMarker( averagePosition( topo ), marker, area );
	} else {
	  RealPos minPos( 9e99, 9e99 );
	  for ( int i = 0; i < world.nodeCount(); i ++ ){
	    minPos[ 0 ] = min( minPos[ 0 ], world.node( i ).pos()[ 0 ] );
	    minPos[ 1 ] = min( minPos[ 1 ], world.node( i ).pos()[ 1 ] );
	    minPos[ 2 ] = max( minPos[ 2 ], world.node( i ).pos()[ 2 ] );
	  }
	  if ( marker > 0 ) world.createRegionMarker( minPos + RealPos( 0.1, 0.1 ), marker, area );
	}
      }

      for ( uint i = 0; i < topo.size(); i ++ ) {
	if ( electrodes[ i ] != 0 ) world.createVIP( topo[ i ].round(1e-14), electrodes[ i ] );
	//world.node( i * splineSegments ).setMarker( -99 );
      }

      if ( objectOnly ){
	for ( int i = 0; i < world.polygonCount(); i ++ ) world.polygon( i ).setMarker( marker );
      }


      world.save( domainFileName );
        } else { // simple 2d - no topo defined
            RealPos size( xdim, ydim );
            if ( fabs( zdim -1.0 ) > TOLERANCE ) size.setY( zdim );
            create2DWorld( size, marker, area ).save( domainFileName );
        }
    } else if ( dimension == 3 ){
    if ( topoFileName != NOT_DEFINED ){
      if ( topoFileName.find( ".poly" ) ){ // 2D input extrude to ydim

	Domain2D world2d; world2d.load( topoFileName );
	Domain3D world;

	RealPos minPos( 9e99, 9e99, -9e99 );
	Polygon front;
	for ( int i = 0; i < world2d.nodeCount(); i ++ ){
	  front.push_back( world.createVIP( RealPos( world2d.node( i ).x(), 0.0, world2d.node( i ).y() ) ) );
	  minPos[ 0 ] = min( minPos[ 0 ], front.back()->pos()[ 0 ] );
	  minPos[ 1 ] = min( minPos[ 1 ], front.back()->pos()[ 1 ] );
	  minPos[ 2 ] = max( minPos[ 2 ], front.back()->pos()[ 2 ] );
	}
	world.stretchOut( front, 0.0, ydim, 0.0 );

	world.facet( 0 ).setBoundaryMarker( MIXED_BC );

 	for ( int i = 0; i < world2d.polygonCount(); i ++ ){
	  world.facet( i + 1 ).setBoundaryMarker( world2d.polygon( i ).marker() );
	}
	world.facet( world.facetCount() -1 ).setBoundaryMarker( MIXED_BC );


	if ( marker > 0 ) world.createRegionMarker( minPos + RealPos( +1, +1, -1 ), marker, area );

	if ( objectOnly ){
	  if ( marker == 0 ) marker = 1;
	  for ( int i = 0; i < world.facetCount(); i ++ ) world.facet( i ).setBoundaryMarker( marker );
	}


	world.save( domainFileName );
      } else {
	cerr << WHERE_AM_I << " Dimension: " << dimension << " topo handling not yet defined here." << endl;
      }
    } else {
      create3DWorld( RealPos( xdim, ydim, zdim ), marker, area ).saveTetgenPolyFile( domainFileName );
    }
  } else {
    cerr << WHERE_AM_I << " Dimension: " << dimension << " undefined." << endl;
  }

  return 0;
}
