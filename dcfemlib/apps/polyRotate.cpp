//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
//

#include <dcfemlib.h>
#include <longoptions.h>
#include <iostream>

#include <elements.h>
#include <domain2d.h>
#include <domain3d.h>

using namespace std;
using namespace DCFEMLib;

template < class ValueType > class Quaternion;

typedef Quaternion< double > RQuaternion;
typedef RealPos RVector3;

RQuaternion getRotation( const RVector3 & src, const RVector3 & dest );
template < class ValueType > class Quaternion{
public:
    Quaternion( ValueType w = 1.0, ValueType i = 0.0, ValueType j = 0.0, ValueType k = 0.0 )
        : re_( w ){
        im_[ 0 ] = i; im_[ 1 ] = j; im_[ 2 ] = k;
    }

    Quaternion( ValueType re, const Pos< ValueType > & im )
        : re_( re ), im_( im ){ }

    Quaternion( const Quaternion < ValueType > & q ) { copy_( q ); }

    Quaternion & operator = ( const Quaternion < ValueType > & q ) {
        if ( this != & q ){
            copy_( q );
        }
        return *this;
    }

    const ValueType operator [] ( const size_t i ) const {
        if ( i == 0 ) return re_; else return im_[ i - 1 ];
    }

    ValueType & operator [] ( const size_t i ){ if ( i == 0 ) return re_; else return im_[ i - 1 ]; }

    Quaternion & operator /= ( const ValueType & s ) { re_ /= s; im_ /= s; return * this;}
    Quaternion & operator *= ( const ValueType & s ) { re_ *= s; im_ /= s; return * this;}

    void createFromAxisAngle( const Pos< ValueType > & axis, double angle ){
        //** The quaternion representing the rotation is
        //**   q = cos(a/2)+sin(a/2)*(x*i+y*j+z*k)
        double ah = 0.5 * angle;
        re_ = std::cos( ah );
        im_ = axis * std::sin( ah );
    }

    template < class Matrix > void rotMatrix( Matrix & rot ) const {
        ValueType x  = 2.0 * im_[ 0 ], y  = 2.0 * im_[ 1 ], z  = 2.0 * im_[ 2 ];

        ValueType wx = x * re_, wy = y * re_, wz = z * re_;
        ValueType xx = x * im_[ 0 ], xy = y * im_[ 0 ], xz = z * im_[ 0 ];
        ValueType yy = y * im_[ 1 ], yz = z * im_[ 1 ], zz = z * im_[ 2 ];

        rot[ 0 ][ 0 ] = 1.0 - ( yy + zz );
        rot[ 0 ][ 1 ] = xy - wz;
        rot[ 0 ][ 2 ] = xz + wy;
        rot[ 0 ][ 3 ] = 0.0;
        rot[ 1 ][ 0 ] = xy + wz;
        rot[ 1 ][ 1 ] = 1.0 - ( xx + zz );
        rot[ 1 ][ 2 ] = yz - wx;
        rot[ 1 ][ 3 ] = 0.0;
        rot[ 2 ][ 0 ] = xz - wy;
        rot[ 2 ][ 1 ] = yz + wx;
        rot[ 2 ][ 2 ] = 1.0 - ( xx + yy );
        rot[ 2 ][ 3 ] = 0.0;
        for ( int i = 0; i < 4; i ++ ) rot[ 3 ][ i ] = 0.0;
    }

    inline ValueType norm() const { return re_ * re_ + im_.abs() * im_.abs(); }

    inline ValueType length() const { return std::sqrt( norm() ); }

    inline void normalise(){ *this /= length(); }

    inline void setRe( const ValueType re ){ re_ = re; }
    inline ValueType re( ) const { return re_; }

    inline void setIm( const Pos < ValueType > & im ) {  im_ = im; }
    inline Pos < ValueType > im( ) const { return im_; }

    friend std::ostream & operator << ( std::ostream & str, const RQuaternion & q );
protected:

    void copy_( const Quaternion < ValueType > & q ){ re_ = q.re(); im_ = q.im(); }

    ValueType         re_;
    Pos < ValueType > im_;
};


RQuaternion getRotation( const RVector3 & src, const RVector3 & dest ){
// Based on Stan Melax's article in Game Programming Gems
    RQuaternion q;
    RVector3 v0( src );
    RVector3 v1( dest );

    if ( v0.abs() < TOLERANCE || v1.abs() < TOLERANCE ) return RQuaternion( 1.0, 0.0, 0.0, 0.0 );

    v0 /= v0.abs();
    v1 /= v1.abs();

    double d = v0.dot( v1 );
  // std::cout << "d " << d << std::endl;
    if ( ::fabs( ( d - 1.0 ) ) < TOLERANCE ) { //** v1 == v2
        return RQuaternion( 1.0, 0.0, 0.0, 0.0 );
    }

    if ( ::fabs( ( d + 1.0 ) ) < TOLERANCE ) { //** v1 == -v2
        RVector3 a( RVector3( 1.0, 0.0, 0.0 ).cross( v0 ) );
        if ( a.abs() < TOLERANCE ){
            a = RVector3( 0.0, 1.0, 0.0 ).cross( v0 );
        }
        a /= a.abs();
        q.createFromAxisAngle( a, PI_ );
//std::cout << q << std::endl;

    } else {
        double s = std::sqrt( (1.0 + d) * 2.0 );
        RVector3 c = v0.cross( v1 ) / s;
        q = RQuaternion( s * 0.5, c );
        q.normalise();
    }
    return q;
}

std::ostream & operator << ( std::ostream & str, const RQuaternion & q ){
    str << "Quaternion( " << q[ 0 ] << ", " << q[ 1 ] << ", "
                            << q[ 2 ] << ", " << q[ 3 ] << " ) ";
    return str;
}

int main( int argc, char * argv[] ){

  LongOptionsList lOpt;
  lOpt.setLastArg("[-RLTx:y:z:] polygon-filename");
  lOpt.setDescription( (string) "rotate a polygonal domain, angles in deg [0 -- 180]" );
  lOpt.insert( "help", no_argument, 'h', "This help" );
  lOpt.insert( "verbose", no_argument, 'v', "Verbose mode" );
  lOpt.insert( "radian", no_argument, 'R', "angles in rad [0]" );
  lOpt.insert( "local", no_argument, 'L', "Rotates in local space." );
  lOpt.insert( "rotToTarget", no_argument, 'T', "Rotates from norm-direction(0.0, 0.0, -1.0) "
                                                "to norm-direction( x, y, z)" );
  lOpt.insert( "x", required_argument, 'x', "double: rotate x [0.0]" );
  lOpt.insert( "y", required_argument, 'y', "double: rotate y [0.0]" );
  lOpt.insert( "z", required_argument, 'z', "double: rotate z [0.0]" );

  bool help = false, verbose = false;

  string domainName( NOT_DEFINED );

  bool radian = false,  local = false, rotToTarget = false;
  double x = 0.0, y = 0.0, z = 0.0;
  int option_char = 0, option_index = 0, tracedepth = 0;
  while ( ( option_char = getopt_long( argc, argv, "?hvRLT"
                                                    "x:y:z:",
                                                    lOpt.long_options(), & option_index ) ) != EOF){
    switch ( option_char ) {
    case '?': lOpt.printHelp( argv[0] ); return 1; break;
    case 'h': lOpt.printHelp( argv[0] ); return 1; break;
    case 'R': radian = true; break;
    case 'L': local = true; break;
    case 'T': rotToTarget = true; break;
    case 'x': x = atof( optarg ); break;
    case 'y': y = atof( optarg ); break;
    case 'z': z = atof( optarg ); break;
    case 'v': verbose = true; break;
    default : cerr << "default value not defined" << endl;
    }
  }
  if ( argc < 2 ) { lOpt.printHelp( argv[0] ); return 1; }

  domainName = argv[argc-1];
  string domainFileName( domainName.substr( 0, domainName.rfind( ".poly" ) ) + ".poly" );
  int dimension = findDomainDimension( domainFileName );

  BaseMesh *domain;
  switch ( dimension ){
  case 2: domain = new Domain2D();
    break;
  case 3: domain = new Domain3D();
    break;
  default: return -1;
  }

  domain->load( domainFileName );
  RealPos avPos( averagePosition( domain->nodePositions() ) );

    if ( local ){
        domain->translate( avPos * -1.0 );
    }

    if ( rotToTarget ) {
        RVector3 src( 0.0, 0.0, -1.0 );
        RVector3 dest( x, y, z );
        RQuaternion q( getRotation( src, dest ) );
       // std::cout << q << std::endl;
        double rot[4][4]; q.rotMatrix( rot );

//         std::cout << "rot " << rot << std::endl;
//         for ( uint i = 0; i < 4; i ++ ){
//             for ( uint j = 0; j < 4; j ++ ){
//                     std::cout << rot[i ][j] << " ";
//             }
//             std::cout << std::endl;
//         }

        domain->transform( rot );
    } else {
        if ( radian ) {
            domain->rotate( x, y, z );
        } else {
            domain->rotate( degToRad( x ), degToRad( y ), degToRad( z ) );
        }
    }

    if ( local ){
        domain->translate( avPos );
    }
    domain->save( domainFileName );

    return 0;
}







